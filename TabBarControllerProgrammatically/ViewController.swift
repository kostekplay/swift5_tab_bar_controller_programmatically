////  ViewController.swift
//  TabBarControllerProgrammatically
//
//  Created on 07/09/2020.
//  Copyright © 2020 kostowski. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    private let button: UIButton = {
        let bt = UIButton()
        bt.setTitle("Log In", for: .normal)
        bt.backgroundColor = .white
        bt.setTitleColor(.black, for: .normal)
        bt.frame = CGRect(x: 0 , y: 0, width: 200, height: 52)
        return bt
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemBlue
        view.addSubview(button)
        button.center = view.center
        button.addTarget(self, action: #selector(didTapButton), for: .touchUpInside)
    }
    
    @objc private func didTapButton() {
        let tabBarVC = UITabBarController()
        
        let v1 = UINavigationController(rootViewController: FirstViewController())
        let v2 = UINavigationController(rootViewController: SecondViewController())
        let v3 = UINavigationController(rootViewController: ThirdViewController())
        let v4 = UINavigationController(rootViewController: FourthViewController())
        let v5 = UINavigationController(rootViewController: FiftyViewController())
        
        v1.title = "brown"
        v2.title = "blue"
        v3.title = "green"
        v4.title = "white"
        v5.title = "orange"
        
        tabBarVC.setViewControllers([v1,v2,v3,v4,v5], animated: false)
        
        guard let items = tabBarVC.tabBar.items else { return }
        
        let images = ["house", "bell", "gear", "star", "house"]
        
        for x in 0..<images.count {
            items[x].image = UIImage(systemName: images[x])
        }
        
        tabBarVC.modalPresentationStyle = .fullScreen
        present ( tabBarVC, animated: true)
    }
}

class FirstViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .brown
    }
}

class SecondViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .blue
        
    }
}

class ThirdViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .green
        
    }
}

class FourthViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        
    }
}

class FiftyViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .orange
        title = "orange"
    }
}

